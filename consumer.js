// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
});

const subscriptionName = 'myclickstreamTrackerSubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);
const uuidv4 = require('uuid/v4');

/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event The Cloud Functions event.
 * @param {!Function} The callback function.
 */
exports.storeClickstreamConsumer = (event, callback) => {
    // The Cloud Pub/Sub Message object.
    const pubsubMessage = event.data;

    // Do something with the message
  console.log(JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString()));
  
    // The kind for the new entity
  const kind = 'clickstream';
  
    // The name/ID for the new entity
    const name = uuidv4();
    console.log(name);
  
    
  // The Cloud Datastore key for the new entity
  const clickstreamDataKey = datastore.key([kind, name]);

  // Prepares the new entity
  const clickstreamData = {
    key: clickstreamDataKey,
    data: JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString()),
  };

  // Saves the entity
  datastore
    .save(clickstreamData)
    .then(() => {
      console.log(`Saved ${clickstreamData.key.name}`);
    })
    .catch(err => {
      console.error('ERROR:', err);
    });
  
    
  
  // "Ack" (acknowledge receipt of) the message
  // message.ack();
  
    // Don't forget to call the callback.
    callback();
};
