// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

// Lists all subscriptions in the current project
pubsubClient
  .getSubscriptions()
  .then(results => {
    const subscriptions = results[0];

    console.log('Subscriptions:');
    subscriptions.forEach(subscription => console.log(subscription.name));
  })
  .catch(err => {
    console.error('ERROR:', err);
  });