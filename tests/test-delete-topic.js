// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Creates a client
const pubsub = new PubSub();

/**
 * TODO(developer): Uncomment the following line to run the sample.
 */
const topicName = 'myTopic';

// Deletes the topic
pubsub
  .topic(topicName)
  .delete()
  .then(() => {
    console.log(`Topic ${topicName} deleted.`);
  })
  .catch(err => {
    console.error('ERROR:', err);
  });