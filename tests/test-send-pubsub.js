// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});
/**
 * HTTP Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 */

const dataBuffer = Buffer.from('{"mouse_position_x":35, "mouse_position_y":240, "sourceUrl": "http://localhost:8000/login", "createdAt":"2017-10-10 01:00:00"}');
const topicName = "myTopic";

pubsubClient
  .topic(topicName)
  .publisher()
  .publish(dataBuffer)
  .then(results => {
    const messageId = results[0];
    console.log(`Message ${messageId} published.`);
  })
  .catch(err => {
    console.error('ERROR:', err);
  });
