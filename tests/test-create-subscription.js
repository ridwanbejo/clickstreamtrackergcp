// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

const topicName = 'clickstreamTracker';
const subscriptionName = 'myclickstreamTrackerSubscription';

// Creates a new subscription
pubsubClient
  .topic(topicName)
  .createSubscription(subscriptionName)
  .then(results => {
    const subscription = results[0];
    console.log(`Subscription ${subscription.name} created.`);
  })
  .catch(err => {
    console.error('ERROR:', err);
  });