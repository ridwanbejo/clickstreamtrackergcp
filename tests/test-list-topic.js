// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Creates a client
const pubsub = new PubSub();

// Lists all topics in the current project
pubsub
  .getTopics()
  .then(results => {
    const topics = results[0];

    console.log('Topics:');
    topics.forEach(topic => console.log(topic.name));
  })
  .catch(err => {
    console.error('ERROR:', err);
  });