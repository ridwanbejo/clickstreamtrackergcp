// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

const subscriptionName = 'mySubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);

// Create an event handler to handle messages
const messageHandler = function(message) {
  // Do something with the message
  console.log(JSON.parse(message.data));

  // "Ack" (acknowledge receipt of) the message
  message.ack();
};

// Create an event handler to handle errors
const errorHandler = function(error) {
  // Do something with the error
  console.error(`ERROR: ${error}`);
};

subscription.on(`message`, messageHandler);
subscription.on(`error`, errorHandler);	

// setTimeout(() => {
//   subscription.removeListener(`message`, messageHandler);
//   subscription.removeListener(`error`, errorHandler);
// }, timeout * 1000);