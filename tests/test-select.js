// Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
});

// Saves the entity
const query = datastore
  .createQuery('clickstream')

datastore.runQuery(query).then(results => {
  // Task entities found.
  const tasks = results[0];

  console.log('Clickstream:');
  tasks.forEach(task => console.log(task));
});