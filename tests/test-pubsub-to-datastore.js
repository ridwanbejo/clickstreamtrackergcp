// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'myproject';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
});

const subscriptionName = 'myclickstreamTrackerSubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);

// Create an event handler to handle messages
const messageHandler = function(message) {
	// Do something with the message
	console.log(JSON.parse(message.data));

	// The kind for the new entity
	const kind = 'clickstream';
	// The name/ID for the new entity
	const name = '12344';
	// The Cloud Datastore key for the new entity
	const clickstreamDataKey = datastore.key([kind, name]);

	// Prepares the new entity
	const clickstreamData = {
	  key: clickstreamDataKey,
	  data: JSON.parse(message.data),
	};

	// Saves the entity
	datastore
	  .save(clickstreamData)
	  .then(() => {
	    console.log(`Saved ${clickstreamData.key.name}`);
	  })
	  .catch(err => {
	    console.error('ERROR:', err);
	  });

	// "Ack" (acknowledge receipt of) the message
	message.ack();
};

// Create an event handler to handle errors
const errorHandler = function(error) {
  // Do something with the error
  console.error(`ERROR: ${error}`);
};

subscription.on(`message`, messageHandler);
subscription.on(`error`, errorHandler);	

// setTimeout(() => {
//   subscription.removeListener(`message`, messageHandler);
//   subscription.removeListener(`error`, errorHandler);
// }, timeout * 1000);