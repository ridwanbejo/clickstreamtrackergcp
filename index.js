// HELP

/*
$ functions deploy storeClickStreamProducer --trigger-http
$ functions call storeClickStreamProducer --data='{"mouse_position_x":35, "mouse_position_y":240, "sourceUrl": "http://localhost:8000/login", "createdAt":"2017-10-10 01:00:00"}'
$ functions deploy helloPubSub --trigger-resource myTopic --trigger-event google.pubsub.topic.publish
*/    

// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

/**
 * Responds to any HTTP request that can provide a "message" field in the body.
 *
 * @param {!Object} req Cloud Function request context.
 * @param {!Object} res Cloud Function response context.
 */
exports.storeClickStreamProducer = (req, res) => {
  if (req.body.mouse_position_x === undefined && req.body.mouse_position_y === undefined) {
    
    res.status(400).send('Bad message!');
  
  } else {
    const dataBuffer = Buffer.from(JSON.stringify(req.body));
    const topicName = "clickstreamTracker";

  pubsubClient
    .topic(topicName)
    .publisher()
    .publish(dataBuffer)
    .then(results => {
      const messageId = results[0];
      console.log(`Message ${messageId} published.`);
    })
    .catch(err => {
      console.error('ERROR:', err);
    });

    res.status(200).send(req.body);
    
  }
};

