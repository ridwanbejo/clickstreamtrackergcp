# Clickstream Tracker System using Google Cloud Platform

## Instructions

### Cloud Function for Producer 

- Create new Cloud Function with name `storeClickStreamProducer` and memory size with `128 MB`
- Choose HTTP Trigger on trigger section to receive HTTP request from client such as from web browser, postman, or from another services.
- Add the `index.js` section please copy the code within index.js on this repository
- Move to `package.json` tab then copy this JSON structure into the text area:

```
{
  "name": "storeClickStreamProducer",
  "version": "0.0.1",
  "dependencies": {
    "@google-cloud/pubsub": "^0.16.4"
  }
}

```

- Cloud Function will install the NPM package that required by Cloud Function automatically
- Type `storeClickStreamProducer` on `Function to execute` input field to pointing the function within the code into Cloud Function runtime. Finally hit the Save Button!
- You can do anything after you create the Function. You can edit, delete, copy, test, and view the logs of the function
- Now your Cloud Function is connected to Stackdriver automatically and this function could send the payload to PubSub. But you need to create the PubSub topic first.

### Topic and Subscription on PubSub

- Open the PubSub menu on left-side menu
- Create new topic with name `clickstreamTracker`
- Add permission with `PubSub Admin` pointing to your account to make your cloud function drop the payload on that PubSub
- Thew new topic is ready to receive subscription from another GCP services
- We will wire the Cloud Function with PubSub in next section.

### Cloud Function for Consumer

- Create new Cloud Function with name `storeClickStreamConsumer` and memory size with `128 MB`
- Choose Cloud Pub/Sub topic wihtin Trigger section
- Choose Pub/Sub that was created in prior section, the topic is `clickstreamTracker`
- Add the `index.js` section please copy the code within consumer.js on this repository
- Move to `package.json` tab then copy this JSON structure into the text area:

```
{
  "name": "storeClickStreamConsumer",
  "version": "0.0.1",
  "dependencies": {
    "@google-cloud/datastore": "^1.3.4",
    "@google-cloud/pubsub": "^0.16.4"
  }
}

```

- Cloud Function will install the NPM package that required by Cloud Function automatically
- Type `storeClickStreamConsumer` on `Function to execute` input field to pointing the function within the code into Cloud Function runtime. Finally hit the Save Button!
- You can do anything after you create the Function. You can edit, delete, copy, test, and view the logs of the function
- Now your Cloud Function is connected to Stackdriver automatically and this function could receive payload from PubSub then store that payload to Cloud Datastore
- Now you can select `View Logs` tabs to see the incoming message from Pub/Sub you have been selected.

### Cloud Datastore

- There are no configuration too much in the dashboard of Cloud Datastore, you can just select what Entities that you want to see
- You can browse the dataset on `clickstream` entities within the dashboard as long the payload is successfully stored on Cloud Datastore

## How it works?

First we send the payload that has structure like this example below:

```
{
    "mouse_position_x": 1241,
    "mouse_position_y": 6678,
    "sourceUrl": "http://localhost:8000/register",
    "createdAt": "2018-03-11 23:00:00"
}
```

Basically the producer is receive payload that contain mouse position, the URL page that mouse is visit, also the event created time from the browser. And then, with HTTP Trigger, Cloud Function that become as a producer receive that payload and queued the message onto Cloud Pub/Sub.

The queued message will be consumed by Cloud Function that become as a consumer, then parse the payload from Pub/Sub, finally the payload will be stored on Cloud Datastore in the `clickstream` entity. Later, you could build anything based on dataset that remain in Cloud Datastore using GQL or Cloud Datastore library.